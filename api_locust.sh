#!/bin/bash

container_api="api_locust"


Delete_Container(){
  if [ $(docker ps -a 2>&1 | grep -c $container_api) == "1" ]; then
    docker rm $container_api -f
    echo "========== Container $container_api deleted! =========="
  fi
}

Delete_Image(){
  if [ $(docker image list 2>&1 | grep -c $container_api) == "1" ]; then
    container=$(docker image rm $container_api)
    echo "========== Image $container_api deleted! =========="
  fi
}

Build_Image(){
  build=$(docker build -t $container_api . 2>&1)
  if [ $(docker image list 2>&1 | grep -c $container_api) == "1" ]; then
    echo "========== Created image $container_api! =========="
  fi
}

Build_Container(){
  netRemove=$(docker network rm swk)
  netAdd=$(docker network create swk)
  if [[ $(docker image list 2>&1 | grep -c $container_api) == "1" && $(docker ps 2>&1 | grep -c "0.0.0.0:80->80") == "0" ]]; then
    build=$(docker run -p 80:80 --network swk -d --name $container_api $container_api)
    echo "========== Created container $container_api! =========="
  else
    echo "========== Port 80 is taken! =========="
  fi
}


######## Delete Container ########
Delete_Container
######## Delete Image ########
Delete_Image
######## Build Image ########
Build_Image
######## Build Container ########
Build_Container
echo "Script Finished"