FROM php:7.3-apache

RUN apt-get update && apt-get install -y zip unzip git libmcrypt-dev && apt-get install libssh2-1-dev -y \
    && docker-php-ext-install pdo_mysql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN apt-get install net-tools -y
RUN apt-get install iputils-ping -y
RUN apt-get install wget -y
RUN a2enmod rewrite

RUN apt-get update && \
    apt-get install -y autoconf pkg-config libssl-dev git libzip-dev zlib1g-dev && \
    pecl install mongodb && docker-php-ext-enable mongodb && \
    pecl install xdebug && docker-php-ext-enable xdebug && \
    docker-php-ext-install -j$(nproc) pdo_mysql zip

RUN wget https://phar.phpunit.de/phpunit-6.5.phar \
    && chmod +x phpunit-6.5.phar \
    && mv phpunit-6.5.phar /usr/local/bin/phpunit

COPY ./src/ /var/www/html
COPY ./settings/apache/000-default.conf /etc/apache2/sites-available/000-default.conf 
COPY ./settings/apache_mods/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

WORKDIR /var/www/html

EXPOSE 80
