import time
import json
from faker import Faker
from locust import HttpUser, task, between
from locust import events
from locust.runners import MasterRunner
import re


class ApiTest(HttpUser):
    def __init__(self, parent):
        super(ApiTest, self).__init__(parent)
        self.token = ''
        self.headers = {}
        self.fake = Faker()

    wait_time = between(3, 10)

    def on_start(self):
        self.token = self.login()
        self.headers = {'authorization': 'Bearer ' + self.token}

    def login(self):
        response = self.client.post(
            "/v1/autenticate/login", json={"username": "lassaut", "password": "qwerty12345"})
        return json.loads(response._content)['token']

    @task(3)
    def send_post(self):
        url = '/v1/facebook/post'
        body = {
            "idPage": 1,
            "message": self.fake.text(50),
            "schedule": 1619999999
        }
        self.client.post(url, body, headers=self.headers)

    @task(1)
    def get_posts(self):
        url = '/v1/facebook/post'
        response = self.client.get(url, headers=self.headers)
        count = json.loads(response._content)[
            'last_page']
        #
        for i in range(2, count):
            json.loads((self.client.get('/v1/facebook/post?page=' + str(i), headers=self.headers))._content)[
                'next_page_url']
