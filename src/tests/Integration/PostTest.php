<?php
declare(strict_types=1);

use App\Models\Page;
use App\Models\Post;

class PostTest extends TestCase
{
    /**
     * @depends AuthenticateTest::testLogin
     */
    public function testSendPost($token){
        $count = Post::all()->count();
        $post = [
            "idPage" => Page::all()->first()->id,
            "message" => "All alright",
            "schedule" => time() + 60
        ];
        $this->post('/v1/facebook/post/',
            $post,
            ['Authorization'=> $token]);
        $content = json_decode($this->response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayHasKey('id',$content);
        $this->assertResponseStatus(201);
        $this->assertNotSame($count,Post::all()->count());
        return $content['id'];
    }

    /**
     * @depends AuthenticateTest::testLogin
     */
    public function testSendPostInvalidPage($token):void{
        $this->post('/v1/facebook/post/',[
            "idPage" => -1,
            "message" => "All alright",
            "schedule" => time() + 60
        ],
        ['Authorization'=> $token]);
        $content = json_decode($this->response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayNotHasKey('id',$content);
        $this->assertResponseStatus(404);
    }

    /**
     * @depends AuthenticateTest::testLogin
     */
    public function testSendPostInvalidDate($token):void{
        $this->post('/v1/facebook/post/',[
            "idPage" => Page::all()->first()->id,
            "message" => "All alright",
            "schedule" => time() - 60
        ],
        ['Authorization'=> $token]);
        $content = json_decode($this->response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayNotHasKey('id',$content);
        $this->assertResponseStatus(400);
    }

    /**
     * @depends AuthenticateTest::testLogin
     * @depends testSendPost
     */
    public function testUpdatePost($token,$id):void{
        $this->put('/v1/facebook/post/' . $id,[
            "message" => "All alright",
            "schedule" => time() + 60
        ],
        ['Authorization'=> $token]);
        $this->assertResponseStatus(200);
    }

    /**
     * @depends AuthenticateTest::testLogin
     * @depends testSendPost
     */
    public function testUpdatePostInvalidDate($token, $id):void{
        $this->put('/v1/facebook/post/' . $id,[
            "message" => "All alright",
            "schedule" => time() - 60
        ],
        ['Authorization'=> $token]);
        $this->assertResponseStatus(400);
    }

    /**
     * @depends AuthenticateTest::testLogin
     * @depends testSendPost
     */
    public function testGetPostById($token, $id):void{
        $this->get('/v1/facebook/post/' . $id,
            ['Authorization'=> $token]);
        $data = json_decode($this->response->content(),true);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('id',$data);
        $this->assertResponseStatus(200);
    }

    /**
     * @depends AuthenticateTest::testLogin
     */
    public function testFailGetPostById($token):void{
        $this->get('/v1/facebook/post/xxx',
            ['Authorization'=> $token]);
        $data = json_decode($this->response->content(),true);
        $this->assertIsArray($data);
        $this->assertArrayNotHasKey('id',$data);
        $this->assertResponseStatus(404);
    }

    /**
     * @depends AuthenticateTest::testLogin
     * @depends testSendPost
     */
    public function testDeletePost($token, $id):void{
        $count = Post::all()->count();
        $this->delete('/v1/facebook/post/' . $id,
        [],
        ['Authorization'=> $token]);
        $this->assertResponseStatus(200);
        $this->assertNotSame($count,Post::all()->count());
    }

    /**
     * @depends AuthenticateTest::testLogin
     */
    public function testDeletePostInvalidId($token):void{
        $this->delete('/v1/facebook/post/0',
        [],
        ['Authorization'=> $token]);
        $this->assertResponseStatus(404);
    }

    /**
     * @depends AuthenticateTest::testLogin
     */
    public function testGetAllPosts($token):void{
        $this->get('/v1/facebook/post/',
            ['Authorization'=> $token]);
        $data = json_decode($this->response->content(),true);
        $this->assertIsArray($data);
        $this->assertArrayHasKey('data',$data);
        $this->assertResponseStatus(200);
    }


}
