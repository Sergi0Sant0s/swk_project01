<?php

use App\Models\Page;

class FacebookTest extends TestCase
{

    /**
     * @depends AuthenticateTest::testLogin
     */
    public function testGetFacebookUrl(string $token) : void{
        $response = $this->get('/v1/facebook/auth',['Authorization'=> $token])->response;
        $content = json_decode($response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayHasKey('url',$content);
        $this->assertResponseStatus(200);
    }
}
