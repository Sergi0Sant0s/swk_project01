<?php
session_start();
use App\Models\Page;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;

class AuthenticateTest extends TestCase
{
    protected $defaultUser=[
        'username' => 'lassaut',
        'password' => 'qwerty12345'
    ];
    protected $data = [
        	"name" => "Sergio",
	        "username"=> "lassaut1",
	        "email" => "sfr_s1@hotmail.com",
	        "password"=> "qwerty12345"
        ];

    protected function mySetUp(): void
    {
        parent::setUp();
        Artisan::call('db:seed', ['--class' => 'UserTableSeeder']);

        //Pages for default user
        $dUser=User::where('username',$this->defaultUser['username'])->first();
        factory(Page::class, 3)
        ->create(['idUser' => $dUser->id]);

        factory(User::class, 3)
        ->create()
        ->each(function ($user) { //Create Pages inside of users
                $pages = factory(Page::class,3)
                    ->create(['idUser' => $user->id])
                    ->each(function ($page) { //Create Posts inside of pages
                        $posts = factory(Post::class,10)
                                ->create([
                                    'idUser' => $page->idUser,
                                    'idPage' => $page->id
                                ]);
                        foreach($posts as $post){
                            $page->posts()->save($post);
                        }
                    });
                foreach($pages as $page){
                    $user->pages()->save($page);
                }
            });
    }

    public function testRegister() {
        $count = User::all()->count();
        $this->post('/v1/autenticate/register',$this->data);
        $this->assertResponseStatus(201);
        $this->assertNotSame($count,User::all()->count());
    }

    /**
     * @depends testRegister
     */
    public function testAlreadyExists(){
        $this->call('POST','/v1/autenticate/register',$this->data);
        $this->assertResponseStatus(422);
    }

    /**
     * @depends testRegister
     */
    public function testLogin(){
        $this->mySetUp();
        $this->post('/v1/autenticate/login',[
            'username' => $this->defaultUser['username'],
            'password' => $this->defaultUser['password']
        ]);
        $content = json_decode($this->response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayHasKey('token',$content);
        $this->assertResponseStatus(200);
        return 'Bearer '. $content['token'];
    }

    /**
     *  @depends testRegister
     */
    public function testFailedLogin(){
        $response = $this->call('POST','/v1/autenticate/login',[
            'username' => 'failfail',
            'password' => 'failfail'
        ]);
        $content = json_decode($response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayNotHasKey('token',$content);
        $this->assertResponseStatus(401);
    }

    /**
     *  @depends testLogin
     */
    public function testGetCurrentUser(string $token){
        $response = $this->get('/v1/profile', ['Authorization'=>$token])->response;
        $content = json_decode($response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayHasKey('user',$content);
        $this->assertResponseStatus(200);
    }

    public function testFailGetCurrentUser(){
        $response = $this->get('/v1/profile', ['Authorization'=>'FAIL'])->response;
        $content = json_decode($response->content(),true);
        $this->assertIsArray($content);
        $this->assertArrayNotHasKey('user',$content);
        $this->assertResponseStatus(401);
    }
}
