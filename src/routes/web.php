<?php

$router->group(['prefix' => 'v1'], function() use ($router){ // /v1
    // Profile
    $router->get('profile',['middleware' => 'auth', 'uses' => 'AutenticateController@profile']);
    // Autenticate
    $router->group(['prefix' => 'autenticate'], function() use ($router){ // /v1/autenticate
        $router->post('login', 'AutenticateController@login');
        $router->post('register', 'AutenticateController@register');
    });

    // Facebook
    $router->group(['prefix'=>'facebook','middleware' => 'auth'], function() use ($router){
        $router->get('auth', 'FacebookController@auth');
        $router->post('callback', 'FacebookController@callbackLogin');
        //Posts
        $router->group(['prefix' => 'post'], function() use ($router){
            $router->get('', 'FacebookController@getAllPosts');
            $router->get('{id}', 'FacebookController@getPostById');
            $router->post('', 'FacebookController@sendPost');
            $router->put('{id}', 'FacebookController@updatePost');
            $router->delete('{id}', 'FacebookController@deletePost');
        });
    });
});
