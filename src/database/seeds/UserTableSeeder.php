<?php

use App\Models\Page;
use App\Models\User;
use App\Models\Post;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Sergio Santos',
            'username' => 'lassaut',
            'password' => password_hash('qwerty12345',PASSWORD_DEFAULT),//'$2y$10$7DmJy16w4qLUn7ZA8MHT3OxBBvWMvVk/r1F1wQbPoDLl7r2RTzcTC',
            'email' => 'sfr_s@hotmail.com',
            'fb_user_id' => '104387925035726',
            'fb_user_token' => 'EAAC62WJjI3MBALnFIDLToQb24fUYgZAhtVeYfxSULZBpKWBtW0gN7fdhZCkv39T88ksYKaU2yMFUjZAd8FRtAH4lbha9SjOAZBzmMWir1gi1xGZCfeIt6ggXK3kXxrHOFUnoJo6XSi0Ay8A1VjDp4JOQSh6mZAZAbMZCSYSTCaubZAWiBk9gzooVy4'
        ]);
    }
}
