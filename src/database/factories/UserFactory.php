<?php

use App\Models\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
            'name' => $faker->name,
            'username' => $faker->userName,
            'email' => $faker->unique()->safeEmail,
            'password' => '$2y$10$0XRTQMqWZyROlh8h/sZNKO.AsPxee892.5OfaD3slCm362cbMj2oW',
    ];
});
