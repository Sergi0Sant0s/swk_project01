<?php

use App\Models\Post;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    $time = $faker->unixTime(time() + (31556926*15));
    if($time > time()){ // Post has not be sended
        return [
                'message' => $faker->text(10),
                'schedule' => $time,
                'state' => 0
            ];
    }else{ // Post has sended
        return [
                'message' => $faker->text(10),
                'schedule' => $time,
                'state' => 1,
                'fb_post_id' => Str::random(150)
            ];
    }

});
