<?php

use App\Models\Page;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Page::class, function (Faker $faker) {
    return [
            'name' => $faker->company,
            'description' => $faker->realText(75),
            'fb_page_id' => Str::random(20),
            'fb_page_token' => Str::random(230)
    ];
});
