<?php
namespace App\Http\Controllers;

use Facebook\Facebook;
use Laravel\Lumen\Routing\Controller as BaseController;


class AuxController extends BaseController
{
    /**
     * Returns facebook object
     *
     * @return Facebook // Returns facebook object
     */
    protected function returnFacebookObj():Facebook{
        return new Facebook([
                    'app_id' => env('FB_APPID'),
                    'app_secret' => env('FB_APPSECRET'),
                    'default_graph_version' => env('FB_GRAPHVERSION'),
        ]);
    }
}
