<?php

namespace App\Http\Controllers;

use App\Exceptions\Users\UserAlreadyExistsException;
use App\Exceptions\Users\UserUnauthorizedException;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class AutenticateController extends BaseController
{

    // Login
    /**
     * Login on api
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request){
        $this->validate($request,
        [
            'username'=> 'required|string|min:3',
            'password' => 'required|string|min:5'
        ]);


        if(count($user = User::where('username',($request->input('username')))->get()) == 0){
            throw new UserUnauthorizedException();
        }


        $credentials = $request->only(['username','password']);

        $user = $user->first();
        if(password_verify($credentials['password'],$user->password)){
            if(password_needs_rehash($user->password,PASSWORD_DEFAULT)){
                $user->update([
                    'password' => password_hash($credentials['password'],PASSWORD_DEFAULT)
                ]);
            }
        }
        if (! $token = Auth::attempt($credentials)) { // Generate token if exists this user
            throw new UserUnauthorizedException(); // Unauthorized
        }
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ], 200); // Autenticated

    }

    /**
     * Register new user
     *
     * @param Request $request
     * @return void
     */
    public function register(Request $request){
        $this->validate($request, [
        'name' => 'required|string',
        'email' => 'required|email',
        'password' => 'required|string|min:5',
        'username' => 'required|string|min:3'
        ]);

        //Verify if user exists
        if(count(User::where('username',($request->input('username')))->get()) != 0){
            throw new UserAlreadyExistsException();
        }

        $body = $request->all();

        $register = array( // Set user object
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'username' => $request->input('username'),
            'password' => password_hash($request->input('password'),PASSWORD_DEFAULT),
        );

        $user = User::Create($register); // Create user
        return response()->json(['user' => $user],201); // Created
    }

    /**
     * Returns user profile
     *
     * @return void
     */
    public function profile(){
        return response()->json(['user' => Auth::user()], 200); // Return current user
    }
}
