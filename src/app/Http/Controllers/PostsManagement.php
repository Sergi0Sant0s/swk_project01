<?php

namespace App\Http\Controllers;

use App\Exceptions\Generics\NotFountException;
use App\Exceptions\Posts\PostAlreadySentException;
use App\Exceptions\Posts\PostInvalidDateException;
use App\Exceptions\Posts\PostInvalidPageException;
use App\Exceptions\Posts\PostInvalidRequestException;
use App\Models\Page;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait PostsManagement{

    /**
     * Send post to facebook
     *
     * @param Request $request
     * @return void
     */
    public function sendPost(Request $request){
        $this->validate($request,[
            'idPage' => 'required|integer',
            'message' => 'required|string',
            'schedule' => 'required|integer'
        ]);

        $body = $request->only(['idPage','message','schedule']);

        $body['schedule'] = $body['schedule'] - ($body['schedule'] % 60);
        $page = Page::find($body['idPage']);


        if($page == NULL)
            throw new PostInvalidPageException();

        if($page->idUser != Auth::user()->id)
            throw new PostInvalidRequestException();

        //Checks if date is over current date
        $time = time();
        $currentTime = $time - ($time % 60);

        if($body['schedule'] <= $currentTime){
            throw new PostInvalidDateException(); // Date is bellow or equal current date
        }

        //Insert post on facebook
        $post = Post::create(array(
            'idPage' => $page->id,
            'idUser' => Auth::id(),
            'message' => $body['message'],
            'schedule' => $body['schedule']
        ));
        return response()->json(['id' => strval($post->id),'message' => 'Scheduled'],201);
    }

    /**
     * Update post scheduled to facebook
     *
     * @param Request $request
     * @return void
     */
    public function updatePost(Request $request, $id){
        $this->validate($request,[
            'message' => 'required|string',
            'schedule' => 'required|integer'
        ]);

        $body = $request->all();

        $post = Post::where('_id',$id)
            ->where('idUser',Auth::id())
            ->first();

        //
        if($post != NULL){ //Checks if id exists
            if($post->sended)
                throw new PostAlreadySentException(); //Post now allowed
            //
            if(date("y/m/d H:i",$body['schedule']) <= date("y/m/d H:i"))
                throw new PostInvalidDateException(); // Date is bellow or equal current date
            //
            $post->update(['message'=>$body['message']]); // Update post
            return response()->json(['message' => 'Updated']); // Updated (200)
        }else{
            throw new NotFountException(); // Not Found
        }
    }

    /**
     * Returns post by id
     *
     * @param Request $request
     * @param [type] $id
     * @return void Returns post
     */
    public function getPostById(Request $request, $id){
        if(($post = Post::find($id)) != NULL){
            return response()->json($post,200,[],JSON_UNESCAPED_SLASHES);
        }
        else{
            throw new NotFountException();
        }
    }

    /**
     * Delete post scheduled to facebook
     *
     * @param [type] $id
     * @return void
     */
    public function deletePost($id){
        //
        if(($post = Post::find($id)) != NULL){ // has id and id exists
                $post->delete();
                return response()->json(['message' => 'Deleted']); // Ok
        }else{
            throw new NotFountException();
        }
    }

    /**
     * Return all posts scheduled to facebook
     *
     * @return void
     */
    public function getAllPosts(){
        return response()->json(Post::where('idUser',Auth::id())->paginate(15),200,[],JSON_UNESCAPED_SLASHES);
    }


}
