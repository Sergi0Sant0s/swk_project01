<?php
namespace App\Http\Controllers;


use App\Exceptions\Facebook\FacebookWithoutLoginException;
use App\Exceptions\Generics\BadRequestException;
use App\Models\Page;
use App\Models\User;
use App\Http\Controllers\PostsManagement;
use Facebook\Facebook;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpClient\CurlHttpClient;

/*
email: jzogormzpm_1614026193@tfbnw.net
password: qwerty.12343
*/

class FacebookController extends AuxController
{
    use PostsManagement;

    /**
     * Facebook authentication
     *
     * @return void //returns login url
     */
    public function auth(Request $request){
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $fb = new Facebook([
                    'app_id' => env('FB_APPID'),
                    'app_secret' => env('FB_APPSECRET'),
                    'default_graph_version' => env('FB_GRAPHVERSION'),
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $permissions = array(
            'user_posts',
            'pages_manage_posts',
            'pages_show_list',
            'pages_manage_metadata'
        );

        //Create login url with redirect page and permissions
        $loginUrl = $helper->getLoginUrl(env('FB_REDIRECT'),$permissions);

        //redirect to facebook
        return response()->json(['url' => $loginUrl], 200, [], JSON_UNESCAPED_SLASHES);
    }

    /**
     * Facebook callback function
     *
     * @return void
     */
    public function callbackLogin(Request $request){
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }

        $this->validate($request,[
            'state' => 'required',
            'code' => 'required',
        ]);

        $_SESSION['FBRLH_code'] = $request->query('code');
        $_SESSION['FBRLH_state'] = $request->query('state');
        //
        $fb = $this->returnFacebookObj();
        //
        $helper = $fb->getRedirectLoginHelper();

        $accessToken = $helper->getAccessToken();

        if (!isset($accessToken)) {
            throw new BadRequestException();
        }

        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId(getenv('FB_APPID'));
        // If you know the user ID this access token belongs to, you can validate it here
        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
        }

        $response = $fb->get('/me?fields=id,name', $accessToken);
        $user = $response->getGraphUser();
        if($accessToken != null && $user['id'] != null){
            if(($getUser = User::find(Auth::id())) != null){
                $getUser->fb_user_token = $accessToken;
                $getUser->fb_user_id = $user['id'];
                $getUser->save();
            }
        }
        return $this->getPages();
    }


    /**
     * Gets pages from facebook
     *
     * @return void
     */
    public function getPages(){
        $user = Auth::user();

        //
        if ($user->fb_user_id != null && $user->fb_user_token != null) {
            $url = getenv('FB_BASEURL') ."/v9.0/$user->fb_user_id/accounts?access_token=$user->fb_user_token";
            $client = new CurlHttpClient();
            $response = $client->request('GET', $url)->getContent();
            $response = (json_decode($response))->data;

            if (count($response) > 0) {
                foreach ($response as $item) {
                    $page = [
                            'idUser' => $user->id,
                            'fb_page_id' => $item->id,
                            'fb_page_token' => $item->access_token,
                            'name' => $item->name,
                            'description' => $item->category
                        ];

                    $getPage = Page::where('idUser',)->pageId($item->id)->first();

                    if ($getPage == null) { // Checks if exists this page
                        Page::create($page); // Create new page
                    } else {
                        $getPage->update($page);
                    }
                }
                return response()->json(['message' => 'Ok']);
            }
        }
        throw new FacebookWithoutLoginException(); // Login on facebook first
    }

}
