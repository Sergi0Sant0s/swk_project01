<?php

namespace App\Models;

use App\Models\Casts\EncryptCast;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $connection = 'mysql';
    protected $table = 'tb_pages';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'idUser',
        'name',
        'fb_page_id',
        'fb_page_token',
        'description'
    ];

    protected $hidden =[
        'fb_page_id',
        'fb_page_token',
    ];

    protected $casts = [
        'fb_page_id' => EncryptCast::class,
        'fb_page_token' => EncryptCast::class
    ];

    public function scopePageId($query,$page_id){
        $app_cipher = env('APP_CIPHER');
        $app_key = env('APP_KEY');
        $ivlen = openssl_cipher_iv_length($app_cipher);
        $iv = env('APP_IV');

        $value = openssl_encrypt($page_id, $app_cipher, $app_key, $options=0, $iv); // Encrypt id of page to test

        return $query->where('fb_page_id', $value);
    }

    public function scopePageToken($query,$page_id){
        $app_cipher = env('APP_CIPHER');
        $app_key = env('APP_KEY');
        $ivlen = openssl_cipher_iv_length($app_cipher);
        $iv = env('APP_IV');

        $value = openssl_encrypt($page_id, $app_cipher, $app_key, $options=0, $iv); // Encrypt token of page to test

        return $query->where('fb_page_token', $value);
    }

    public function posts(){
        return $this->hasMany(Post::class,'idPage','id');
    }


}

