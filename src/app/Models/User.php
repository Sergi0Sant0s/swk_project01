<?php

namespace App\Models;

use App\Models\Casts\EncryptCast;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\Crypt;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    protected $connection = 'mysql';
    protected $table = 'tb_users';
    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
        'username',
        'password',
        'email',
        'fb_user_id',
        'fb_user_token'
    ];

    protected $hidden = [
        'password',
        'email',
        'fb_user_id',
        'fb_user_token'
    ];


    protected $casts = [
        'email' => EncryptCast::class,
        'fb_user_id' => EncryptCast::class,
        'fb_user_token' => EncryptCast::class,
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function pages(){
        return $this->hasMany(Page::class,'idUser','id');
    }
}
