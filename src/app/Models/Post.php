<?php

namespace App\Models;

use App\Models\Casts\EncryptCast;
use Jenssegers\Mongodb\Eloquent\Model;

class Post extends Model
{
    protected $connection = 'mongodb';
    protected $collection = 'post';

    public $timestamps = false;

    protected $attributes = [
        'state' => 0
    ];

    protected $maps = [
        '_id' => 'id'
    ];

    protected $appends = [
        'id'
    ];

    protected $fillable = [
        'id',
        'idUser',
        'idPage',
        'message',
        'media',
        'schedule',
        'state',
        'fb_post_id',
        'start_processing'
    ];

    protected $hidden =[
        '_id',
        'fb_post_id'
    ];

    protected $casts = [
        'fb_post_id' => EncryptCast::class
    ];

    function page()
    {
      return $this->hasOne(Page::class,'id','idPage');
    }

}
