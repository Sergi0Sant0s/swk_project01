<?php

namespace App\Exceptions\Generics;

use Exception;

class BadRequestException extends Exception
{
    public function __construct()
    {
        $this->message = 'Bad Request';
        $this->code = 400;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
