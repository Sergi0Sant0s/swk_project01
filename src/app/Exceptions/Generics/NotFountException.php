<?php

namespace App\Exceptions\Generics;

use Exception;

class NotFountException extends Exception
{
    public function __construct()
    {
        $this->message = 'Not Found';
        $this->code = 404;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
