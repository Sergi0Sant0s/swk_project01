<?php

namespace App\Exceptions\Posts;

use Exception;

class PostInvalidDateException extends Exception
{
    public function __construct()
    {
        $this->message = 'Schedule is bellow or equal current date';
        $this->code = 400;
    }


    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
