<?php

namespace App\Exceptions\Posts;

use Exception;

class PostAlreadySentException extends Exception
{
    public function __construct()
    {
        $this->message = 'The post already has been sent';
        $this->code = 406;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
