<?php

namespace App\Exceptions\Posts;

use Exception;

class PostInvalidRequestException extends Exception
{
    public function __construct()
    {
        $this->message = 'Not Acceptable';
        $this->code = 406;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
