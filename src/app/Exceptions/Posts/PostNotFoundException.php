<?php

namespace App\Exceptions\Posts;

use Exception;

class PostNotFoundException extends Exception
{
    public function __construct()
    {
        $this->message = 'The post is not found.';
        $this->code = 404;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
