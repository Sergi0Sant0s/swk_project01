<?php

namespace App\Exceptions\Users;

use Exception;

class UserAlreadyExistsException extends Exception
{
    public function __construct()
    {
        $this->message = 'The user already exists.';
        $this->code = 422;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
