<?php

namespace App\Exceptions\Users;

use Exception;

class UserUnauthorizedException extends Exception
{
    public function __construct()
    {
        $this->message = 'Unauthorized.';
        $this->code = 401;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
