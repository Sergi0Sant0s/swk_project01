<?php

namespace App\Exceptions\Facebook;

use Exception;

class FacebookWithoutLoginException extends Exception
{
    public function __construct()
    {
        $this->message = 'Login on facebook first';
        $this->code = 400;
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response()->json(['message'=>$this->message],$this->code);
    }
}
